// https://adventofcode.com/2018/day/25
use std::str::FromStr;

#[derive(Debug, Clone)]
struct Star {
    x: i32,
    y: i32,
    z: i32,
    w: i32
}

impl std::ops::Sub for &Star {
    type Output = Star;
    fn sub(self, rhs: Self) -> Self::Output {
        Self::Output {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
            z: self.z - rhs.z,
            w: self.w - rhs.w,
        }
    }
}

impl Star {
    fn new(x: i32, y: i32, z: i32, w: i32) -> Star {
        Star {
            x: x,
            y: y,
            z: z,
            w: w
        }
    }
    fn sum(&self) -> u32{
        abs(self.x) as u32 + abs(self.y) as u32 + abs(self.z) as u32 + abs(self.w) as u32
    }
}

// absolute value, always positive
fn abs(input: i32) -> i32{
    if input.is_negative() {
        return input * -1;
    } else {
        return input;
    }
}

fn manhattan_distance(lhs: &Star, rhs: &Star) -> u32 {
    let distance1 = lhs - rhs;
    let distance2 = rhs-lhs;
    if distance1.sum() < distance2.sum() {
        return distance1.sum();
    }
    distance2.sum()
}

#[derive(Debug, Clone)]
struct Constellation(Vec<Star>);

impl Constellation {
    // tries to add star to constellation, returns false if too far away
    fn add_star(&mut self, star: &Star) -> bool {
        for i in &self.0 {
            if manhattan_distance(&star, &i) <= 3 {
                self.0.push(star.clone());
                return true;
            }
        }
        false
    }
    fn new(start: Vec<Star>) -> Constellation{
        Constellation(start)
    }
}

fn input_from_text(input: String) -> Vec<Star> {
    let mut stars = Vec::<Star>::new();
    for line in input.lines() {
        let mut current_coor: [i32; 4] = [0; 4];
        let coors_str: Vec<&str> = line.split(",").collect();
        for mut i in 0..coors_str.len() {
            let number = coors_str[i];
            match i32::from_str(number) {
                Ok(n) => {
                    current_coor[i % 4] = n;
                },
                Err(e) => {
                    println!("parse error {} in {}", e, coors_str[i]);
                    i += 4 - (i % 4);
                }
            }
            if i == 3{
                stars.push(
                    Star::new(
                        current_coor[0],
                        current_coor[1],
                        current_coor[2],
                        current_coor[3],
                    )
                )
            }
        }
    }
    stars
}


fn main() {
    let file = std::fs::read_to_string("./input").unwrap();
    let stars = input_from_text(file);
    println!("{:#?}", stars);
    let mut constellations: Vec<Constellation> = Vec::new();
    constellations.push(Constellation::new(vec![stars[0].clone()]));
    let stars: Vec<Star> = stars[1..].to_vec();
    for star in stars {
        for i in 0..constellations.len() {
            if constellations[i].add_star(&star) {
                break;
            }
            if i == constellations.len() - 1 {
                constellations.push(Constellation::new(vec![star.clone()]));
            }
        }
    }
    println!("{}", constellations.len());

}
